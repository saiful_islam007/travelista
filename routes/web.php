<?php

use App\Http\Controllers\Admin\BlogController;
use App\Http\Middleware\OnlyAdmin;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Site\HomeController;
use App\Http\Controllers\Admin\RoomController;
use App\Http\Controllers\Admin\HotelController;
use App\Http\Controllers\Admin\PackageController;
use App\Http\Controllers\Admin\CategoryController;
use App\Http\Controllers\Admin\DashboardController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/




Route::get('/home',[HomeController::class,'index'])->name('site.home');
Route::get('/all-package',[PackageController::class,'all_package']);
Route::get('/single-package/{id}',[HomeController::class,'single'])->name('site.single');

Route::get('/all-hotel',[HotelController::class,'all_hotel']);
Route::get('/single-hotel/{id}',[HotelController::class,'single']);

Route::get('/all-blog',[BlogController::class,'all_blog']);
Route::get('/single-blog/{id}',[BlogController::class,'single']);

Route::prefix('/admin')->middleware(['auth',OnlyAdmin::class/*,'verified'*/])->group(function(){
    Route::get('/dashboard',[DashboardController::class,'index'])->name('admin.dashboard');
    Route::resource('/categories',CategoryController::class);
    Route::resource('/packages',PackageController::class);
    Route::resource('/hotels',HotelController::class);
    Route::resource('/rooms',RoomController::class);
    Route::resource('/blogs',BlogController::class);
});

require __DIR__.'/auth.php';
