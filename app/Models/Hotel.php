<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Hotel extends Model
{
    use HasFactory;

    public function hotel__images()
    {
        return $this->hasMany(Hotel_Image::class);
    }

    public function rooms()
    {
        return $this->hasMany(Room::class);
    }
}
