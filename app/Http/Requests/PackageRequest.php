<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PackageRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required',
            'location' => 'required',
            'duration' => 'required',
            'package_type' => 'required',
            'about_country' => 'nullable',
            'departure_return_location' => 'nullable',
            'price_per_person' => 'numeric|nullable',
            'departure_time' => 'required',
            'itinerary' => 'required'
            
        ];
    }
}
