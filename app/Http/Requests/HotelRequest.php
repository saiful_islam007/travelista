<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class HotelRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required',
            'hotel_district' => 'required',
            'location' => 'required',
            'star' => 'required|numeric',
            'short_description' => 'required',
            'wifi' => 'required',
            'air_condition' => 'required',
            'pool' => 'required',
            'gym' => 'required',
            'room_service' => 'required',
            'parking' => 'required',
            'laundry' => 'required',
            'restaurant' => 'required',
            'breakfast' => 'required',
    
        ];
    }
}
