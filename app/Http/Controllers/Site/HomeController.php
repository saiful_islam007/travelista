<?php

namespace App\Http\Controllers\Site;

use App\Models\Package;
use App\Models\Category;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Blog;

class HomeController extends Controller
{
     public function index()
    {
        $package=Package::get();
        $data['package_list']=$package;

        $blog=Blog::inRandomOrder()->take(5)->get();
        $data['blog']=$blog;

        $latest_data=Package::take(3)->orderBy('created_at','desc')->get();
        $data['latest_package']=$latest_data;

        $cheap_package=Package::where('package_type','Cheap Packages')->take(6)->orderBy('price_per_person','asc')->get();
        $data['cheap_package']=$cheap_package;

        $luxury_package=Package::where('package_type','Luxury Packages')->take(6)->orderBy('price_per_person','asc')->get();
        $data['luxury_package']=$luxury_package;

        $camping_packages=Package::where('package_type','Camping Packages')->take(6)->get();
        $data['camping_packages']=$camping_packages;
        return view('site.layouts.home',$data);
    }

    public function single($id)
    {
        $package=Package::find($id);
        $data['single_package']=$package;

        $random=Package::inRandomOrder()->take(5)->get();
        $data['random_packages']=$random;
        return view('site.layouts.single_package',$data);
    }


}
