<?php

namespace App\Http\Controllers\Admin;

use App\Models\Room;
use App\Models\Hotel;
use App\Models\Image;
use App\Models\Hotel_Image;
use Illuminate\Http\Request;
use App\Http\Requests\HotelRequest;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\File;

class HotelController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $hotel=Hotel::get();
        $data['hotels']=$hotel;
        return view('admin.hotel.index',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.hotel.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(HotelRequest $request)
    {
        try {
            $hotel=new Hotel();
            $hotel->title=$request->title;
            $hotel->hotel_district=$request->hotel_district;
            $hotel->location=$request->location;
            $hotel->star=$request->star;
            $hotel->short_description=$request->short_description;
            $hotel->wifi=$request->wifi;
            $hotel->air_condition=$request->air_condition;
            $hotel->pool=$request->pool;
            $hotel->gym=$request->gym;
            $hotel->room_service=$request->room_service;
            $hotel->parking=$request->parking;
            $hotel->laundry=$request->laundry;
            $hotel->restaurant=$request->restaurant;
            $hotel->breakfast=$request->breakfast;
            
            if($request->hasFile('featured_image')){
                $path = $request->file('featured_image')->store('hotel_images','public');
            }
            else{
                $path= null;
            }
            $hotel->featured_image=$path;
            $hotel->save();
            
            if($request->hasFile('images')){
                foreach ($request->file('images') as $image) {
                    $path = $image->store('hotel_images','public');
                    $image=new Hotel_Image();
                    $image->path=$path;
                    $image->hotel_id=$hotel->id;
                    $image->save();             
                }
            }
            
            flash('Successfully Created')->success();
            return redirect('/admin/hotels');
        } catch (\Throwable $th) {
            flash('Something went worng'.$th->getMessage())->error();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $hotel=Hotel::find($id);
        $data['hotel']=$hotel;
        return view('admin.hotel.edit',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $hotel=Hotel::find($id);
        if(!$hotel)
        {
            return false;
        }
            $hotel->title=$request->title;
            $hotel->hotel_district=$request->hotel_district;
            $hotel->location=$request->location;
            $hotel->star=$request->star;
            $hotel->short_description=$request->short_description;
            $hotel->wifi=$request->wifi;
            $hotel->air_condition=$request->air_condition;
            $hotel->pool=$request->pool;
            $hotel->gym=$request->gym;
            $hotel->room_service=$request->room_service;
            $hotel->parking=$request->parking;
            $hotel->laundry=$request->laundry;
            $hotel->restaurant=$request->restaurant;
            $hotel->breakfast=$request->breakfast;

        if($request->hasFile('featured_image')){
            $destination ='storage/'.$hotel->featured_image;
            if(File::exists($destination))
            {
                File::delete($destination);
            }
            $path = $request->file('featured_image')->store('hotel_images','public');
            $hotel->featured_image=$path;
        }
        $hotel->save();
        if($request->hasFile('images')){
            foreach ($hotel->hotel__images as $item) {
                $destination1='storage/'.$item->path;
                $data=Hotel_Image::find($item->id);
                if(File::exists($destination1))
                 {
                     File::delete($destination1);
                     $data->delete();
                     
                 }

                
                 
             }
            foreach ($request->file('images') as $image) {
                $path = $image->store('hotel_images','public');
                    $image=new Hotel_Image();
                    $image->path=$path;
                    $image->hotel_id=$hotel->id;
                    $image->save();         
            }
        }
        flash('Successfully updated')->success();
        return redirect('/admin/hotels');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Hotel::find($id)->hotel__images()->delete();
        // Hotel::find($id)->categories()->detach();
        $hotel=Hotel::find($id);
        if(!$hotel)
        {
            flash('no data found')->error();
            return redirect('/admin/hotels');
        }else{
            $hotel->delete();
            flash('Successfully deleted')->success();
            return redirect()->back();
        }
    }

    public function all_hotel()
    {
        $hotel=Hotel::inRandomOrder()->get();
        $data['hotels']=$hotel;
        return view('site.layouts.all_hotel',$data);
    }

    public function single($id)
    {
        $hotel=Hotel::find($id);
        $data['single_hotel']=$hotel;
        $random=Hotel::inRandomOrder()->take(5)->get();
        $data['random_hotel']=$random;

        $room=Room::where('hotel_id',$hotel->id)
        ->orderBy('price_per_night','desc')
        ->get();
        $data['rooms']=$room;
        return view('site.layouts.single_hotel',$data);
    }
    
}
