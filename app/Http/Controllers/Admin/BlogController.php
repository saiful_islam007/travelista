<?php

namespace App\Http\Controllers\Admin;

use Carbon\Carbon;
use App\Models\Blog;
use App\Models\Category;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Blog_Image;
use Illuminate\Support\Facades\Auth;

class BlogController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $blog=Blog::get();
        $data['blog_list']=$blog;
        return view('admin.blog.index',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $category=Category::get();
        $data['categories']=$category;
        return view('admin.blog.create',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            
            $blog=new Blog();
            $blog->title=$request->title;
            $blog->publishing_date=Carbon::now();
            $blog->author= Auth::user()->name;
            $blog->description=$request->description;
            if($request->hasFile('featured_image')){
                $path = $request->file('featured_image')->store('blog_images','public');
            }
            else{
                $path= null;
            }
            $blog->featured_image=$path;
            $blog->save();
            $blog->categories()->attach($request->categories);
            if($request->hasFile('images')){
                foreach ($request->file('images') as $image) {
                    $path = $image->store('blog_images','public');
                    $image=new Blog_Image();
                    $image->path=$path;
                    $image->blog_id=$blog->id;
                    $image->save();             
                }
            }
            
            flash('Successfully Created')->success();
            return redirect('/admin/blogs');
        }
         catch (\Throwable $th) {
            flash('Something went worng'.$th->getMessage())->error();
        }
    }
    

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function all_blog()
    {
        $blog=Blog::get();
        $data['blogs']=$blog;
        return view('site.layouts.all_blog',$data);
    }

    public function single($id)
    {
        $blog=Blog::find($id);
        $data['single_blog']=$blog;

        $blog1=Blog::get();
        $data['blogs']=$blog1;
        return view('site.layouts.single_blog',$data);
    }
}
