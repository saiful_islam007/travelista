<?php

namespace App\Http\Controllers\Admin;

use App\Models\Room;
use App\Models\Hotel;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;

class RoomController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $room=Room::get();
        $data['rooms']=$room;
        return view('admin.room.index',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $hotel=Hotel::get();
        $data['hotel']=$hotel;
        return view('admin.room.create',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        try {
            $room=New Room();
            $room->title=$request->title;
            $room->bed=$request->bed;
            $room->bed_type=$request->bed_type;
            $room->sleep=$request->sleep;
            $room->price_per_night=$request->price_per_night;
            $room->sleep=$request->sleep;
            $room->hotel_id=$request->hotel_id;
    
            if($request->hasFile('room_image')){
                $path = $request->file('room_image')->store('room_images','public');
            }
            else{
                $path= null;
            }
            $room->room_image=$path;
            $room->save();
            flash('Successfully Created')->success();
            return redirect('/admin/rooms');
        } catch (\Throwable $th) {
            flash('Something went worng'.$th->getMessage())->error();

        }
       
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $room=Room::find($id);
        $data['hotel']=Hotel::get();
        $data['room']=$room;
        return view('admin.room.edit',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $room=Room::find($id);
        if(!$room)
        {
            return false;
        }
            $room->title=$request->title;
            $room->bed=$request->bed;
            $room->bed_type=$request->bed_type;
            $room->sleep=$request->sleep;
            $room->price_per_night=$request->price_per_night;
            $room->sleep=$request->sleep;
            $room->hotel_id=$request->hotel_id;
    
            if($request->hasFile('room_image')){
                $destination ='storage/'.$room->room_image;
                if(File::exists($destination))
                {
                    File::delete($destination);
                }
                $path = $request->file('room_image')->store('room_images','public');
                $room->room_image=$path;
            }
            $room->save();
            flash('Successfully updated')->success();
            return redirect('/admin/rooms');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        
        
        $room=Room::find($id);
        if(!$room)
        {
            flash('no data found')->error();
            return redirect('/admin/rooms');
        }else{
            $room->delete();
            flash('Successfully deleted')->success();
            return redirect()->back();
        }
    }

    
}
