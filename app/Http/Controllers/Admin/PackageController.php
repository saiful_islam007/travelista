<?php

namespace App\Http\Controllers\Admin;

use App\Models\Image;
use App\Models\Package;
use App\Models\Category;
use Illuminate\Http\Request;
use PhpParser\Node\Stmt\TryCatch;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\File;
use App\Http\Requests\PackageRequest;

class PackageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $package=Package::get();
        $data['package_list']=$package;
        return view('admin.package.index',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $category=Category::get();
        $data['categories']=$category;
        return view('admin.package.create',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PackageRequest $request)
    {
        try {
            $package=new Package();
            $package->title=$request->title;
            $package->location=$request->location;
            $package->duration=$request->duration;
            $package->package_type=$request->package_type;
            $package->about_country=$request->about_country;
            $package->departure_return_location=$request->departure_return_location;
            $package->departure_time=$request->departure_time;
            $package->price_per_person=$request->price_per_person;
            $package->itinerary=$request->itinerary;
            

            if($request->hasFile('featured_image')){
                $path = $request->file('featured_image')->store('package_images','public');
            }
            else{
                $path= null;
            }
            $package->featured_image=$path;
            $package->save();
            $package->categories()->attach($request->categories);
            if($request->hasFile('images')){
                foreach ($request->file('images') as $image) {
                    $path = $image->store('package_images','public');
                    $image=new Image();
                    $image->path=$path;
                    $image->package_id=$package->id;
                    $image->save();             
                }
            }
            flash('Successfully Created')->success();
            return redirect('/admin/packages');
        } catch (\Throwable $th) {
            flash('Something went worng'.$th->getMessage())->error();
        }
     
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $package=Package::find($id);
        $category=Category::get();
        $data['categories']=$category;
        $data['package']=$package;
        return view('admin.package.edit',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $package=Package::find($id);
        if(!$package)
        {
            return false;
        }
        $package->title=$request->title;
        $package->location=$request->location;  
        $package->duration=$request->duration;
        $package->package_type=$request->package_type;
        $package->about_country=$request->about_country;
        $package->departure_return_location=$request->departure_return_location;
        $package->departure_time=$request->departure_time;
        $package->price_per_person=$request->price_per_person;
        $package->minimum_person=$request->minimum_person;
        $package->itinerary=$request->itinerary;

        if($request->hasFile('featured_image')){
            $destination ='storage/'.$package->featured_image;
            if(File::exists($destination))
            {
                File::delete($destination);
            }
            $path = $request->file('featured_image')->store('package_images','public');
            $package->featured_image=$path;
        }
        $package->save();
        $package->categories()->sync($request->categories);
        if($request->hasFile('images')){
            foreach ($package->images as $item) {
                $destination1='storage/'.$item->path;
                $data=Image::find($item->id);
                if(File::exists($destination1))
                 {
                     File::delete($destination1);
                     $data->delete();
                     
                 }  
                 
             }
            foreach ($request->file('images') as $image) {
                $path = $image->store('package_images','public');
                $image=new Image();
                $image->path=$path;
                $image->package_id=$package->id;
                $image->save();             
            }
        }
        flash('Successfully updated')->success();
        return redirect('/admin/packages');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Package::find($id)->images()->delete();
        Package::find($id)->categories()->detach();
        $package=Package::find($id);
        if(!$package)
        {
            flash('no data found')->error();
            return redirect('/admin/packages');
        }else{
            $package->delete();
            flash('Successfully deleted')->success();
            return redirect()->back();
        }
    }

    public function all_package()
    {
        $package=Package::get();
        $data['all_package']=$package;
        $random=Package::inRandomOrder()->take(3)->get();
        $data['random_packages']=$random;
        return view('site.layouts.all_package',$data);
    }
}
