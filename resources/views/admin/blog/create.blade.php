@extends('admin.layouts.app')

@section('styles')
<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
@endsection

@section('page_title')
<div class="row mb-2">
  <div class="col-sm-6">
    <h1>Blog</h1>
  </div>
  <div class="col-sm-6">
    <ol class="breadcrumb float-sm-right">
      <li class="breadcrumb-item"><a href="{{ url('/admin/dashboard') }}">Dashboard</a></li>
      <li class="breadcrumb-item active">Blog</li>
    </ol>
  </div>
</div>
@endsection

@section('content')
<div class="card"> 
    <div class="card-header">
      <h3 class="card-title">Add Blog</h3>
    </div>
    <div class="card-body">
        <form action="{{ route('blogs.store') }}" method="POST" enctype="multipart/form-data" id="form">
            @csrf
            <div class="card-body">

              <div class="form-group">
                <label for="cars">Select package category:</label>
                    <select  name="categories[]" class="form-group categories col-4"  multiple="multiple">
                      @foreach ($categories as $item)
                      <option value="{{ $item->id }}">{{ $item->name }}</option>
                      @endforeach
                    </select>
              </div>

              <div class="form-group">
                <label for="exampleInputEmail1">Title</label>
                <input type="text" class="form-control" name="title" id="title" value="{{ old('title') }}" placeholder="Enter blog title">
              </div>
              @error('title')
                 <p class="text-danger">{{ $message }}</p>
              @enderror

              {{-- <div class="form-group">
                <label for="exampleInputEmail1">Location</label>
                <input type="date" class="form-control" name="publishing_date" id="publishing_date" value="{{ old('publishing_date') }}" placeholder="Enter publish date">
              </div>
              @error('publishing_date')
                 <p class="text-danger">{{ $message }}</p>
              @enderror --}}

              <div class="form-group">
                <label for="exampleInputEmail1">Description</label>
                <textarea name="description" id="" cols="150" rows="10"></textarea>
              </div>
              @error('description')
                 <p class="text-danger">{{ $message }}</p>
              @enderror

              <div class="form-group">
                <label for="exampleInputEmail1">Featured Image</label>
                <input type="file" name="featured_image">
              </div>

              <div class="form-group">
                <label for="exampleInputEmail1">Images</label>
                <input type="file" name="images[]" multiple>
              </div>

            </div>
         
            <!-- /.card-body -->

            <div class="card-footer">
              <button type="submit" class="btn btn-primary">Create</button>
            </div>
          </form>
    </div>
  </div>
@endsection

@section('scripts')
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>

<script>
  $(document).ready(function() {
    $('.categories').select2();
});
</script>
@endsection

