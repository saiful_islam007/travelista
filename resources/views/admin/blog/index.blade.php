@extends('admin.layouts.app')

@section('content')
<div class="card">
    <div class="card-header">
        <h3 class="card-title">Package List</h3>
        <div class="card-tools">
          <a  class="btn btn-success" href="{{ url('/admin/blogs/create') }}">Add New Blog</a>
        </div>
    </div>
    <div class="card-body">
       <table>
        <thead>
          <tr>
            <th>Category</th>
            <th>Title</th>
            <th>Publish Date</th>
            <th>Author</th>
            <th>Description</th>
            <th>Featured_Image</th>
            <th>Images</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody>
            @foreach ($blog_list as $blog)
            <tr>
                <td>
                  @foreach ( $blog->categories  as $item)
                      <p>{{ $item->name }}</p>
                  @endforeach
                </td>
                <td>{{ $blog->title }}</td>
                <td>{{ $blog->publishing_date }}</td>
                <td>{{ $blog->author }}</td>
                <td class="mt-5" style="width:100%; height: 300px;display: block; overflow:scroll">{{ $blog->description }}</td>
                
                
                <td><img src="{{ asset("storage/$blog->featured_image") }}" width="100px"></td>
                <td>
                  @foreach ($blog->blog__images as $item)
                  <img src="{{ asset("storage/$item->path") }}" width="100px">
                  @endforeach
                </td>
                <td>
                  <div class="btn-group" role="group">
                    <a href="{{ url("admin/blogs/$blog->id/edit") }}" class="btn btn-primary btn-sm">Update</a>
                   
    
                    <form action="{{ url("/admin/blogs/$blog->id") }}" method="POST" onsubmit="return confirm('Do you really want to delete this category?');">
                        @csrf
                        @method('delete')
                        <input type="submit" value="Delete" class="btn btn-danger btn-sm ml-1">
                    </form>
                  </div>
                </td>
              </tr>
            @endforeach 
        </tbody>
      </table> 
    </div>
    
  </div>
@endsection

