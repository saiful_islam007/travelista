@extends('admin.layouts.app')

@section('content')
<div class="card">
    <div class="card-header col-lg-12">
        <h3 class="card-title">Hotel List</h3>
        <div class="card-tools">
          <a  class="btn btn-success" href="{{ url('/admin/hotels/create') }}">Add New Hotels</a>
        </div>
    </div>
    <div class="card-body" style="display: block; overflow:scroll">
        <table class="table table-bordered  ">
            <thead>
              <tr>
                <th>Name</th>
                <th>Hotel District</th>
                <th>Location</th>
                <th>Type</th>
                <th>Short Description</th>
                <th>WiFi</th>
                <th>Air Condition</th>
                <th>Pool</th>
                <th>Gym</th>
                <th>Room Service</th>
                <th>Parking</th>
                <th>Laundry</th>
                <th>Restaruant</th>
                <th>Breakfast</th>
                <th>Feature Image</th>
                <th>Images</th>
                <th class="col-3">Action</th>
              </tr>
            </thead>
            <tbody>
                 @foreach ($hotels as $hotel)
                <tr>
                    <td>{{ $hotel->title }}</td>
                    <td>{{ $hotel->hotel_district }}</td>
                    <td>{{ $hotel->location }}</td>
                    <td>{{ $hotel->star }} star</td>
                    <td>{{ $hotel->short_description }}</td>
                    <td>{{ $hotel->wifi }}</td>
                    <td>{{ $hotel->air_condition }}</td>
                    <td>{{ $hotel->pool }}</td>
                    <td>{{ $hotel->gym }}</td>
                    <td>{{ $hotel->room_service }}</td>
                    <td>{{ $hotel->parking }}</td>
                    <td>{{ $hotel->laundry }}</td>
                    <td>{{ $hotel->restaurant }}</td>
                    <td>{{ $hotel->breakfast }}</td>
                    <td><img src="{{ asset("storage/$hotel->featured_image") }}" width="100px"></td>
                    <td>
                      @foreach ($hotel->hotel__images as $item)
                      <img src="{{ asset("storage/$item->path") }}" width="100px">
                      @endforeach
                    </td>
                    <td class="col-3">
                      <div class="btn-group" role="group">
                        <a href="{{ url("admin/hotels/$hotel->id/edit") }}" class="btn btn-primary btn-sm">Update</a>
                        
        
                        <form action="{{ url("/admin/hotels/$hotel->id") }}" method="POST" onsubmit="return confirm('Do you really want to delete this category?');">
                            @csrf
                            @method('delete')
                            <input type="submit" value="Delete" class="btn btn-danger btn-sm ml-1">
                        </form>
                      </div>
                    </td>
                 </tr>
                @endforeach 
            </tbody>
          </table>
    </div>
    
  </div>
@endsection

