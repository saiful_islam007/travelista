@extends('admin.layouts.app')

@section('page_title')
<div class="row mb-2">
  <div class="col-sm-6">
    <h1>Hotel</h1>
  </div>
  <div class="col-sm-6">
    <ol class="breadcrumb float-sm-right">
      <li class="breadcrumb-item"><a href="{{ url('/admin/dashboard') }}">Dashboard</a></li>
      <li class="breadcrumb-item active">Hotel</li>
    </ol>
  </div>
</div>
@endsection

@section('content')
<div class="card"> 
    <div class="card-header">
      <h3 class="card-title">Add Hotel</h3>
    </div>
    <div class="card-body">
        <form action="{{ route('hotels.store') }}" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="card-body">
                <div class="form-group">
                    <label for="exampleInputEmail1">Hotel Name</label>
                    <input type="text" class="form-control" name="title"  value="{{ old('title') }}" placeholder="Enter Hotel Name">
                </div>
                @error('title')
                <p class="text-danger">{{ $message }}</p>
                @enderror
            
                <label>Hotel District</label>
                <div class="form-group">
                    <textarea name="hotel_district" cols="120" rows="2">{{ old('hotel_district') }}</textarea>
                    @error('hotel_district')
                        <p class="text-danger">{{ $message }}</p>
                    @enderror
                </div>

                <label>Location</label>
                <div class="form-group">
                    <textarea name="location" cols="120" rows="2">{{ old('location') }}</textarea>
                    @error('location')
                        <p class="text-danger">{{ $message }}</p>
                    @enderror
                </div>

                <label>Star</label>
                <div class="form-group">
                    <input type="text" class="form-control" name="star" value="{{ old('star') }}" placeholder="Enter Hotel Star">
                    @error('star')
                        <p class="text-danger">{{ $message }}</p>
                    @enderror
                </div>

                <label>Short_description</label>
                <div class="form-group">
                    <textarea name="short_description"  cols="120" rows="5">{{ old('short_description') }}</textarea>
                </div>
                @error('short_description')
                    <p class="text-danger">{{ $message }}</p>
                @enderror

                <div class="form-group">
                    <label for="cars">WiFI</label>
                        <select name="wifi" class="form-select col-2" aria-label="Default select example" value="{{ old('wifi') }}">
                                <option value="">Select a Option</option>
                                <option value="Yes">Yes</option>
                                <option value="NO">NO</option>
                        </select>
                  </div>
                  @error('wifi')
                     <p class="text-danger">{{ $message }}</p>
                  @enderror

                  <div class="form-group">
                    <label for="cars">Air Condition</label>
                        <select name="air_condition" class="form-select col-2" aria-label="Default select example" value="{{ old('air_condition') }}">
                                <option>Select a Option</option>    
                                <option value="Yes">Yes</option>
                                <option value="NO">NO</option>
                        </select>
                  </div>
                  @error('air_condition')
                     <p class="text-danger">{{ $message }}</p>
                  @enderror

                  <div class="form-group">
                    <label for="cars">Pool</label>
                        <select name="pool" class="form-select col-2" aria-label="Default select example" value="{{ old('pool') }}">
                                <option>Select a Option</option>
                                <option value="Yes">Yes</option>
                                <option value="NO">NO</option>
                        </select>
                  </div>
                  @error('pool')
                     <p class="text-danger">{{ $message }}</p>
                  @enderror
                  
                  <div class="form-group">
                    <label for="cars">GYM</label>
                        <select name="gym" class="form-select col-2" aria-label="Default select example" value="{{ old('gym') }}">
                                <option>Select a Option</option>
                                <option value="Yes">Yes</option>
                                <option value="NO">NO</option>
                        </select>
                  </div>
                  @error('gym')
                     <p class="text-danger">{{ $message }}</p>
                  @enderror

                  <div class="form-group">
                    <label for="cars">Room Service</label>
                        <select name="room_service" class="form-select col-2" aria-label="Default select example" value="{{ old('room_service') }}">
                                <option>Select a Option</option>
                                <option value="Yes">Yes</option>
                                <option value="NO">NO</option>
                        </select>
                  </div>
                  @error('room_service')
                     <p class="text-danger">{{ $message }}</p>
                  @enderror

                  <div class="form-group">
                    <label for="cars">Parking</label>
                        <select name="parking" class="form-select col-2" aria-label="Default select example" value="{{ old('parking') }}">
                                <option>Select a Option</option>
                                <option value="Yes">Yes</option>
                                <option value="NO">NO</option>
                        </select>
                  </div>
                  @error('parking')
                     <p class="text-danger">{{ $message }}</p>
                  @enderror

                  <div class="form-group">
                    <label for="cars">Laundry</label>
                        <select name="laundry" class="form-select col-2" aria-label="Default select example" value="{{ old('laundry') }}">
                                <option>Select a Option</option>
                                <option value="Yes">Yes</option>
                                <option value="NO">NO</option>
                        </select>
                  </div>
                  @error('laundry')
                     <p class="text-danger">{{ $message }}</p>
                  @enderror

                  <div class="form-group">
                    <label for="cars">Restaruant</label>
                        <select name="restaurant" class="form-select col-2" aria-label="Default select example" value="{{ old('restaurant') }}">
                                <option>Select a Option</option>
                                <option value="Yes">Yes</option>
                                <option value="NO">NO</option>
                        </select>
                  </div>
                  @error('restaurant')
                     <p class="text-danger">{{ $message }}</p>
                  @enderror

                  <div class="form-group">
                    <label for="cars">Breakfast</label>
                        <select name="breakfast" class="form-select col-2" aria-label="Default select example" value="{{ old('breakfast') }}">
                                <option>Select a Option</option>
                                <option value="Yes">Yes</option>
                                <option value="NO">NO</option>
                        </select>
                  </div>
                  @error('breakfast')
                     <p class="text-danger">{{ $message }}</p>
                  @enderror

                  <div class="form-group">
                    <label for="exampleInputEmail1">Featured Image</label>
                    <input type="file" name="featured_image">
                  </div>
                  @error('featured_image')
                     <p class="text-danger">{{ $message }}</p>
                  @enderror
    
                  <div class="form-group">
                    <label for="exampleInputEmail1">Images</label>
                    <input type="file" name="images[]" multiple>
                  </div>
                  @error('images')
                     <p class="text-danger">{{ $message }}</p>
                  @enderror
    

            </div>
            <!-- /.card-body -->

            <div class="card-footer">
              <button type="submit" class="btn btn-primary">Create</button>
            </div>
          </form>
    </div>
  </div>
@endsection
