@extends('admin.layouts.app')

@section('content')
<div class="card">
    <div class="card-header">
        <h3 class="card-title">Package List</h3>
        <div class="card-tools">
          <a  class="btn btn-success" href="{{ url('/admin/packages/create') }}">Add New Package</a>
        </div>
    </div>
    <div class="card-body">
      <table>
        <thead>
          <tr>
            <th>Category</th>
            <th>Title</th>
            <th>Location</th>
            <th>Duration</th>
            <th>Package Type</th>
            <th>About City</th>
            <th>Depature/Return Location</th>
            <th>Departure Time</th>
            <th>Price Per Person</th>
            <th>Itinerary</th>
            <th>Featured_Image</th>
            <th>Images</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody>
            @foreach ($package_list as $package)
            <tr>
                <td>
                  @foreach ( $package->categories  as $item)
                      <p>{{ $item->name }}</p>
                  @endforeach
                </td>
                <td>{{ $package->title }}</td>
                <td>{{ $package->location }}</td>
                <td>{{ $package->duration }}</td>
                <td>{{ $package->package_type }}</td>
                <td class="mt-5" style="width:100%; height: 300px;display: block; overflow:scroll">{{ $package->about_country }}</td>
                <td>{{ $package->departure_return_location }}</td>
                <td>{{ $package->departure_time }}</td>
                <td>{{ $package->price_per_person }}</td>
                @php
                $size=$package->itinerary;
                $product_size=explode('|',$size);
                @endphp
                <td style="width:100%; height:300px;display: block; overflow:scroll">
                  @foreach ($product_size as $one_size)
                      {{ $one_size }}<hr>
                  @endforeach
                </td>
                
                <td><img src="{{ asset("storage/$package->featured_image") }}" width="100px"></td>
                <td>
                  @foreach ($package->images as $item)
                  <img src="{{ asset("storage/$item->path") }}" width="100px">
                  @endforeach
                </td>
                <td>
                  <div class="btn-group" role="group">
                    <a href="{{ url("admin/packages/$package->id/edit") }}" class="btn btn-primary btn-sm">Update</a>
                   
    
                    <form action="{{ url("/admin/packages/$package->id") }}" method="POST" onsubmit="return confirm('Do you really want to delete this category?');">
                        @csrf
                        @method('delete')
                        <input type="submit" value="Delete" class="btn btn-danger btn-sm ml-1">
                    </form>
                  </div>
                </td>
              </tr>
            @endforeach 
        </tbody>
      </table>
    </div>
    
  </div>
@endsection

