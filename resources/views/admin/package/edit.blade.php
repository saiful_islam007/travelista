@extends('admin.layouts.app')

@section('styles')
<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
@endsection

@section('page_title')
<div class="row mb-2">
  <div class="col-sm-6">
    <h1>Category</h1>
  </div>
  <div class="col-sm-6">
    <ol class="breadcrumb float-sm-right">
      <li class="breadcrumb-item"><a href="{{ url('/admin/dashboard') }}">Dashboard</a></li>
      <li class="breadcrumb-item active">Package</li>
    </ol>
  </div>
</div>
@endsection

@section('content')
<div class="card"> 
    <div class="card-header">
      <h3 class="card-title">Add Package</h3>
    </div>
    <div class="card-body">
        <form action="{{ url("admin/packages/$package->id") }}" method="POST" enctype="multipart/form-data" id="form">
            @csrf
            @method('PUT')
            <div class="card-body">

                <label for="cars">Select package category:</label>
              <div class="form-group"> 
                    <select  name="categories[]" class="form-group categories col-4"  multiple="multiple">
                      @foreach ($categories as $item)
                      <option value="{{ $item->id }}">{{ $item->name }}</option>
                      @endforeach
                    </select>
                    {{-- <span class="text-danger error-text package_type_error"><strong></strong></span> --}}
              </div>

              <div class="form-group">
                <label for="exampleInputEmail1">Title</label>
                <input type="text" class="form-control" name="title" id="title" value="{{ $package->title }}" placeholder="Enter Package title">
                {{-- <span class="text-danger error-text title_error"></span> --}}
              </div>
              @error('title')
                 <p class="text-danger">{{ $message }}</p>
              @enderror

              <div class="form-group">
                <label for="exampleInputEmail1">Location</label>
                <input type="text" class="form-control" name="location" id="location" value="{{ $package->location }}" placeholder="Enter Package location">
                {{-- <span class="text-danger error-text title_error"></span> --}}
              </div>
              @error('location')
                 <p class="text-danger">{{ $message }}</p>
              @enderror

              <div class="form-group">
                <label for="exampleInputEmail1">Duration</label>
                <input type="text" class="form-control" name="duration" id="duration" value="{{ $package->duration }}" placeholder="Enter Package duration">
                {{-- <span class="text-danger error-text duration_error"></span> --}}
              </div>
              @error('duration')
                 <p class="text-danger">{{ $message }}</p>
              @enderror

              <div class="form-group">
                <label for="cars">Choose a Package:</label>
                    <select id="#" name="package_type" class="form-select" aria-label="Default select example" value="{{ $package->package_type }}">
                            <option value="Cheap Packages">Cheap Packages</option>
                            <option value="Luxury Packages">Luxury Packages</option>
                            <option value="Camping Packages">Camping Packages</option>
                    </select>
                    {{-- <span class="text-danger error-text package_type_error"><strong></strong></span> --}}
              </div>
              @error('package_type')
                 <p class="text-danger">{{ $message }}</p>
              @enderror

              <label>Abount Country</label>
              <div class="form-group">
                <textarea name="about_country" id="" cols="120" rows="5">{{ $package->about_country }}</textarea>
                {{-- <span class="text-danger error-text about_country_error"></span> --}}
              </div>
              @error('about_country')
                 <p class="text-danger">{{ $message }}</p>
              @enderror

              <label>DEPARTURE/RETURN LOCATION</label>
              <div class="form-group">
                <textarea name="departure_return_location" cols="120" rows="1">{{ $package->departure_return_location }}</textarea>
              </div>
             

              <div class="form-group">
                <label for="exampleInputEmail1"> DEPARTURE TIME</label>
                    <input type="text" class="form-control" name="departure_time" id="departure_time" value="{{ $package->departure_time }}" placeholder="Enter departure time">
                    {{-- <span class="text-danger error-text departure_time_error"><strong></strong></span> --}}
              </div>
              @error('departure_time')
                 <p class="text-danger">{{ $message }}</p>
              @enderror

              <div class="form-group">
                <label for="exampleInputEmail1">Price per person</label>
                    <input type="number" class="form-control" name="price_per_person" id="price_per_person" value="{{ $package->price_per_person }}" placeholder="price per person">
                    {{-- <span class="text-danger error-text price_per_person_error"></span> --}}
              </div>
               @error('price_per_person')
                 <p class="text-danger">{{ $message }}</p>
              @enderror 

              <label for="exampleInputEmail1">Itinerary</label>
              <div class="form-group">
                <textarea name="itinerary" id="" cols="150" rows="10">{{ $package->itinerary}}</textarea>
              </div>

              <div class="form-group">
                <label for="exampleInputEmail1">Featured Image</label>
                <input type="file" name="featured_image">
                <img src="{{ asset("storage/$package->featured_image") }}" width="100px">
              </div>

              <div class="form-group">
                <label for="exampleInputEmail1">Images</label>
                <input type="file" name="images[]" multiple>
                @foreach ($package->images as $item)
                  <img src="{{ asset("storage/$item->path") }}" width="100px">
                  @endforeach
              </div>

            </div>
         
            <!-- /.card-body -->

            <div class="card-footer">
              <button type="submit" class="btn btn-primary">update</button>
            </div>
          </form>
    </div>
  </div>
@endsection

@section('scripts')
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>

@php
    $category_ids=[];
@endphp
@foreach ($package->categories as $category)
    @php
        array_push($category_ids,$category->id);
    @endphp
@endforeach

<script>
  $(document).ready(function() {
    $('.categories').select2();
    data =[];
    data =<?php echo json_encode($category_ids); ?>
    // $('.categories').val(data);
    $('.categories').val(data).trigger('change');
});
</script>
@endsection

