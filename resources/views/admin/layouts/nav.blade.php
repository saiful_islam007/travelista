<nav class="mt-2">
    <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
      <li class="nav-item">
        <a href="{{ url('/admin/categories') }}" class="nav-link">
          <i class="fas fa-folder"></i>
          <p>
            Category
          </p>
        </a>
      </li>
      <li class="nav-item">
        <a href="{{ url('/admin/packages') }}" class="nav-link">
          <i class="fas fa-folder"></i>
          <p>
            Packages
          </p>
        </a>
      </li>
      <li class="nav-item">
        <a href="{{ url('/admin/hotels') }}" class="nav-link">
          <i class="fas fa-folder"></i>
          <p>
            Hotels
          </p>
        </a>
      </li>
      <li class="nav-item">
        <a href="{{ url('/admin/rooms') }}" class="nav-link">
          <i class="fas fa-folder"></i>
          <p>
            Rooms
          </p>
        </a>
      </li>
      <li class="nav-item">
        <a href="{{ url('/admin/blogs') }}" class="nav-link">
          <i class="fas fa-folder"></i>
          <p>
            Blogs
          </p>
        </a>
      </li>
    </ul>
  </nav> 