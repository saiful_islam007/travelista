@extends('admin.layouts.app')

@section('page_title')
<div class="row mb-2">
  <div class="col-sm-6">
    <h1>Category</h1>
  </div>
  <div class="col-sm-6">
    <ol class="breadcrumb float-sm-right">
      <li class="breadcrumb-item"><a href="{{ url('/admin/dashboard') }}">Dashboard</a></li>
      <li class="breadcrumb-item active">Room</li>
    </ol>
  </div>
</div>
@endsection

@section('content')
<div class="card"> 
    <div class="card-header">
      <h3 class="card-title">Add Room</h3>
    </div>
    <div class="card-body">
        <form action="{{ url("admin/rooms/$room->id") }}" method="POST" enctype="multipart/form-data">
            @csrf
            @method('PUT')
            <div class="card-body">

                <div class="form-group">
                    <label for="cars">Select hotel:</label>
                        <select  name="hotel_id" class="form-group categories col-4">
                          @foreach ($hotel as $item)
                          <option value="{{ $item->id }}" {{ $room->hotel_id == $item->id ? 'selected':''}}>{{ $item->title }}</option>
                          @endforeach
                        </select>
                  </div>


              <div class="form-group">
                <label for="exampleInputEmail1">Title</label>
                <input type="text" class="form-control" name="title" value="{{ $room->title }}" placeholder="Enter Room title">
              </div>
              @error('title')
              <p class="text-danger">{{ $message }}</p>
              @enderror

                <div class="form-group">
                    <label for="exampleInputEmail1">Bed No</label>
                    <input type="number" class="form-control" name="bed" value="{{ $room->bed }}" placeholder="Enter Bed Number">
                </div>
                  @error('bed')
                  <p class="text-danger">{{ $message }}</p>
                  @enderror

                <div class="form-group">
                    <label for="cars">Bed Type</label>
                    <select name="bed_type" class="form-select col-2" aria-label="Default select example">
                                <option value="">Select a Option</option>
                                <option value="single" {{ $room->bed_type=="single" ? 'selected' : '' }}>Single</option>
                                <option value="double" {{ $room->bed_type=="double" ? 'selected' : '' }}>Double</option>
                    </select>
                </div>
                  @error('bed_type')
                  <p class="text-danger">{{ $message }}</p>
                  @enderror

                <div class="form-group">
                    <label for="exampleInputEmail1">Sleep Person</label>
                    <input type="number" class="form-control" name="sleep" value="{{ $room->sleep }}" placeholder="Enter Bed Number">
                </div>
                  @error('bed')
                  <p class="text-danger">{{ $message }}</p>
                  @enderror

                <div class="form-group">
                    <label for="exampleInputEmail1">Price Per Night</label>
                    <input type="text" class="form-control" name="price_per_night" value="{{ $room->price_per_night }}" placeholder="Enter price per night">
                </div>
                @error('name')
                 <p class="text-danger">{{ $message }}</p>
                @enderror

                <div class="form-group">
                    <label for="exampleInputEmail1">Image</label>
                    <input type="file" name="room_image">
                    <td><img src="{{ asset("storage/$room->room_image") }}" width="100px"></td>
                </div>
                  @error('room_image')
                     <p class="text-danger">{{ $message }}</p>
                  @enderror
     

            </div>
         
            <!-- /.card-body -->

            <div class="card-footer">
              <button type="submit" class="btn btn-primary">Create</button>
            </div>
          </form>
    </div>
  </div>
@endsection
