@extends('admin.layouts.app')

@section('content')
<div class="card">
    <div class="card-header">
        <h3 class="card-title">Room List</h3>
        <div class="card-tools">
          <a  class="btn btn-success" href="{{ url('/admin/rooms/create') }}">Add New Room</a>
        </div>
    </div>
    <div class="card-body">
        <table class="table table-bordered ">
            <thead>
              <tr>
                <th>Hotel name</th>
                <th>Name</th>
                <th>Bed No</th>
                <th>Bed Type</th>
                <th>Sleeping Person</th>
                <th>Price Per Night</th>
                <th>Image</th>
                <th class="col-3">Action</th>
              </tr>
            </thead>
            <tbody>
                @foreach ($rooms as $room)
                <tr>
                  <td>{{ $room->hotel->title }}</td>
                    <td>{{ $room->title }}</td>
                    <td>{{ $room->bed }}</td>
                    <td>{{ $room->bed_type }}</td>
                    <td>{{ $room->sleep }}</td>
                    <td>{{ $room->price_per_night }}</td>
                    <td><img src="{{ asset("storage/$room->room_image") }}" width="100px"></td>
                    <td class="col-3">
                      <div class="btn-group" role="group">
                        <a href="{{ url("admin/rooms/$room->id/edit") }}" class="btn btn-primary btn-sm">Update</a>
                        
        
                        <form action="{{ url("/admin/rooms/$room->id") }}" method="POST" onsubmit="return confirm('Do you really want to delete this room?');">
                            @csrf
                            @method('delete')
                            <input type="submit" value="Delete" class="btn btn-danger btn-sm ml-1">
                        </form>
                      </div>
                    </td>
                  </tr>
                @endforeach  
            </tbody>
          </table>
    </div>
    
  </div>
@endsection

