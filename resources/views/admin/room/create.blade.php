@extends('admin.layouts.app')

@section('page_title')
<div class="row mb-2">
  <div class="col-sm-6">
    <h1>Category</h1>
  </div>
  <div class="col-sm-6">
    <ol class="breadcrumb float-sm-right">
      <li class="breadcrumb-item"><a href="{{ url('/admin/dashboard') }}">Dashboard</a></li>
      <li class="breadcrumb-item active">Room</li>
    </ol>
  </div>
</div>
@endsection

@section('content')
<div class="card"> 
    <div class="card-header">
      <h3 class="card-title">Add Room</h3>
    </div>
    <div class="card-body">
        <form action="{{ route('rooms.store') }}" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="card-body">

                <div class="form-group">
                    <label for="cars">Select hotel:</label>
                        <select  name="hotel_id" class="form-group categories col-4">
                          @foreach ($hotel as $item)
                          <option value="{{ $item->id }}">{{ $item->title }}</option>
                          @endforeach
                        </select>
                  </div>


              <div class="form-group">
                <label for="exampleInputEmail1">Title</label>
                <input type="text" class="form-control" name="title" value="{{ old('title') }}" placeholder="Enter Room title">
              </div>
              @error('name')
              <p class="text-danger">{{ $message }}</p>
              @enderror

                <div class="form-group">
                    <label for="exampleInputEmail1">Bed No</label>
                    <input type="number" class="form-control" name="bed" value="{{ old('bed') }}" placeholder="Enter Bed Number">
                </div>
                  @error('bed')
                  <p class="text-danger">{{ $message }}</p>
                  @enderror

                <div class="form-group">
                    <label for="cars">Bed Type</label>
                    <select name="bed_type" class="form-select col-2" aria-label="Default select example" value="{{ old('bed_type') }}">
                                <option value="">Select a Option</option>
                                <option value="single">Single</option>
                                <option value="double">Double</option>
                    </select>
                </div>
                  @error('bed_type')
                  <p class="text-danger">{{ $message }}</p>
                  @enderror

                <div class="form-group">
                    <label for="exampleInputEmail1">Sleep Person</label>
                    <input type="number" class="form-control" name="sleep" value="{{ old('bed') }}" placeholder="Enter Bed Number">
                </div>
                  @error('bed')
                  <p class="text-danger">{{ $message }}</p>
                  @enderror

                <div class="form-group">
                    <label for="exampleInputEmail1">Price Per Night</label>
                    <input type="text" class="form-control" name="price_per_night" value="{{ old('price_per_night') }}" placeholder="Enter price per night">
                </div>
                @error('name')
                 <p class="text-danger">{{ $message }}</p>
                @enderror

                <div class="form-group">
                    <label for="exampleInputEmail1">Image</label>
                    <input type="file" name="room_image">
                </div>
                  @error('room_image')
                     <p class="text-danger">{{ $message }}</p>
                  @enderror
     

            </div>
         
            <!-- /.card-body -->

            <div class="card-footer">
              <button type="submit" class="btn btn-primary">Create</button>
            </div>
          </form>
    </div>
  </div>
@endsection
