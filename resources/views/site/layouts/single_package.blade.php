@extends('site.layouts.app')

@section('content')
<!-- start banner Area -->
<section class="relative about-banner">	
	<div class="overlay overlay-bg"></div>
	<div class="container">				
		<div class="row d-flex align-items-center justify-content-center">
			<div class="about-content col-lg-12">
				<h1 class="text-white">
					Tour Package				
				</h1>	
				<p class="text-white link-nav"><a href="index.html">Home </a> <span class="lnr lnr-arrow-right"></span><a href="blog-home.html">Package </a></p>
			</div>	
		</div>
	</div>
</section>
<!-- End banner Area -->					  

<!-- Start post-content Area -->
<section class="post-content-area single-post-area">
	<div class="container">
		<div class="row">
			<div class="col-lg-9 posts-list">
				<div class="single-post row">
					<div class="col-lg-12">
						<div class="feature-img">
							<h3 class="mt-20 mb-20">{{ $single_package->title }}</h3>
							<div class="inline-element" >
								<div>
									<i class="fa-solid fa-clock fa-2x"></i>
									<span class="ml-2 fw-bold"><b class="text-black">{{ $single_package->duration }}</b></span>
									<span class="float-right"><b class="text-black"><h6>Category:</h6></b>
									@foreach ( $single_package->categories  as $item)
										<strong class="text-success">{{ $item->name }},</strong>
									@endforeach
								</span>
								</div>
							</div>
							<img class="img-fluid" style="width=920px;height:450px;" src="{{ asset("storage/$single_package->featured_image") }}">
						</div>	
						
						
						<div class="row">
							<div id="small-img" class="col-xs-12 col-sm-12 col-md-12 col-lg-12 center">
							  <ul class="ml-5">
								  @foreach ($single_package->images as $item)
								  <li class="image_li">
									<img style="width=150px;height:150px;" src="{{ asset("storage/$item->path") }}" class="img-responsive_imge inline-block" alt="Responsive image" />
								  </li>
								  @endforeach
							  </ul>
							</div>
						  </div>
					</div>
				</div>
			
				{{-- <div class="clearfix"> </div> --}}

				<h3>Description</h3>
				<hr>
				<div class="bootstrap-tab animated wow slideInUp animated" data-wow-delay=".5s" style="visibility: visible; animation-delay: 0.5s; animation-name: slideInUp;">
					<div class="bs-example bs-example-tabs" role="tabpanel" data-example-id="togglable-tabs">
						
						<div id="myTabContent" class="tab-content">
							<ul id="myTab" class="nav nav-tabs" role="tablist">
								<i class="fal fa-check-square fa-2x"></i>
								<li role="presentation" class="active ml-1 mt-1">
									<a href="#home" id="home-tab" role="tab" data-toggle="tab" aria-controls="home" aria-expanded="true">
										<h5 class="text-primary">Tour Details</h5>
									</a>
								</li>
								<i class="fa-solid fa-calendar-days fa-2x ml-4"></i>
								<li class="ml-1 mt-1" role="presentation">
									<a href="#profile" role="tab" id="profile-tab" data-toggle="tab" aria-controls="profile">
										<h5 class="text-primary">Itinerary</h5>
									</a>
								</li>
							</ul>
							
							<div role="tabpanel" class="tab-pane fade in active bootstrap-tab-text" id="home" aria-labelledby="home">
								<div class="mt-3">
									<h4 class="text-center">About {{ $single_package->location }}</h4>
									<p class="mt-2"><strong>{{ $single_package->about_country }}</strong></p>
									<hr>
									<table class="tours-tabs_table">
										<tbody>
										<tr>
											<th class="text-black">DEPARTURE/RETURN LOCATION &emsp;</th>
											<td><b class="text-black">{{$single_package->departure_return_location }}</b> </td>
										</tr>
										<tr></tr>
										<tr></tr>
										<tr>
											<th class="text-black">DEPARTURE TIME</th>
											<td><b class="text-black">{{ $single_package->departure_time }}</b></td>
										</tr>
										<tr class="mt-5"></tr>
										
										<tr>
										<th class="text-black">INCLUDED</th>
										<td>
										<table>
										<tbody>
										<tr>
										<td></td>
										<td class="text-black"><i class="fa fa-check icon-tick icon-tick--on"></i>Road Bus Ticket</td>
										<td></td>
										<td class="text-black"><i class="fa fa-check icon-tick icon-tick--on"></i>Accommodations</td>
										</tr>
										<tr>
										<td></td>
										<td class="text-black"><i class="fa fa-check icon-tick icon-tick--on "></i>Car/ Jeep Transportation</td>
										<td></td>
										<td class="text-black"><i class="fa fa-check icon-tick icon-tick--on"></i>All Meal</td>
										</tr>
										</tbody>
										</table>
										</td>
										</tr>
										<tr>
										<td><b class="text-danger">NOT INCLUDED</b></td>
										<td>
										<table>
										<tbody>
										<tr>
										<td class="text-black"><i class="fa fa-times icon-tick icon-tick--off"></i>Entrance fees</td>
										<td class="text-black"><i class="fa fa-times icon-tick icon-tick--off"></i>Visa fees</td>
										</tr>
										<tr>
										<td class="text-black"><i class="fa fa-times icon-tick icon-tick--off"></i>Personal cost</td>
										<td class="text-black"><i class="fa fa-times icon-tick icon-tick--off"></i>Guide&nbsp;gratuity</td>
										</tr>
										</tbody>
										</table>
										</td>
										</tr>
										<tr>
											<th><h4 class="text-success">Price Per Person</h4></th>
											<td><h4 class="text-danger">{{ $single_package->price_per_person }}</h4><h4>TAKA</h4></td>
										</tr>
										</tbody>
										</table>
										<hr>
										<h6>Minimum booking {{ $single_package->minimum_person }} Pax to ensure this tour</h6>
											
								</div>
							</div>

							
							<div role="tabpanel" class="tab-pane fade bootstrap-tab-text" id="profile" aria-labelledby="profile-tab">
								<div mt-3>
									<h4 class="mt-4 text-center"><u>TOUR ITINERARY</u></h4>
								</div>
								<div></div>

								@php
								$day=$single_package->itinerary;
								$package_days=explode('|',$day);
								$i=1
								@endphp
								@foreach ($package_days as $one_day)
									<div>
										<h6>DAY {{ $i }}</h6>
										<p>{{ $one_day }}</p>
									</div>
									@php
										$i++;
									@endphp	
								@endforeach

							</div>
						</div>
					</div>
				</div>
	</div>
	<div class="inline-block col-lg-3 ">
			<div class="textwidget"><h4>Need Discover Holidays Help?</h4>
				<p>We would be more than happy to help you. Our team remains stand by for 24/7 at your service.</p>
				<h3 style="color: #931e1e;">For Custom Package or Group Tour Contact us:</h3>
				<p>                                <span></span></p>
				<h3><i class="fa fa-phone"></i> +88 01883050240</h3>
				<p></p>
				<p><span><i class="fa fa-envelope"></i> info@travelistabd.com</span></p>
			</div>
			<div>
				@foreach ($random_packages as $package)
				<div class="single-destination relative mt-2">
					<div class="thumb relative">
						<div class="overlay overlay-bg"></div>
						<img class="img-fluid" style="height:230px;" src="{{ asset("storage/$package->featured_image") }}">
					</div>
					<div class="desc">	
						<a href="{{ url("/single-package/$package->id") }}" class="price-btn">{{ $package->price_per_person }}</a>			
						<p><h6 class="text-warning">{{ $package->location }}</h6></p>
						<span class="ml-2 fw-bold"><b>{{ $package->duration }}</b></span>			
					</div>	
				</div>
		@endforeach	
			</div>
	</div>
	
</div>
</div>	
</section>
@endsection