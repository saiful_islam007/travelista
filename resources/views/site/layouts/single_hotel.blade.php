@extends('site.layouts.app')

@section('content')
<!-- start banner Area -->
<section class="relative about-banner">	
	<div class="overlay overlay-bg"></div>
	<div class="container">				
		<div class="row d-flex align-items-center justify-content-center">
			<div class="about-content col-lg-12">
				<h1 class="text-white">
				 Hotel				
				</h1>	
				<p class="text-white link-nav"><a href="index.html">Home </a> <span class="lnr lnr-arrow-right"></span><a href="blog-home.html">Hotel </a></p>
			</div>	
		</div>
	</div>
</section>
<!-- End banner Area -->					  
				           
            <!-- Start post-content Area -->
            <section class="post-content-area single-post-area">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-9 posts-list">
                            <div class="single-post row">
                                <div class="col-lg-12">
                                    <div class="feature-img">
                                        <h3 class="mt-20 mb-20">{{ $single_hotel->title }}</h3>
                                        <div class="inline-element" >
                                            <div>
                                                <i class="fa-solid fa-clock fa-2x"></i>
                                                <span class="ml-2 fw-bold"><b class="text-black">{{ $single_hotel->location }}</b></span>
                                                <span class="float-right"><b class="text-black"><h5>{{ $single_hotel->star }} star</h5></b>
                                            </div>
                                        </div>
                                        <img class="img-fluid" style="width=920px;height:450px;" src="{{ asset("storage/$single_hotel->featured_image") }}">
                                    </div>	
                                    
                                    
                                    <div class="row">
                                        <div id="small-img" class="col-xs-12 col-sm-12 col-md-12 col-lg-12 center">
                                          <ul class="ml-5">
                                            @foreach ($single_hotel->hotel__images as $item)
                                            <li class="image_li">
                                              <img style="width=150px;height:150px;" src="{{ asset("storage/$item->path") }}" class="img-responsive_imge inline-block" alt="Responsive image" />
                                            </li>
                                            @endforeach                              
                                         </ul>
                                        </div>
                                      </div>
                                </div>
                            </div>
                        
                            
            
                            <h3>Description</h3>
                            <hr>
                            <div class="bootstrap-tab animated wow slideInUp animated" data-wow-delay=".5s" style="visibility: visible; animation-delay: 0.5s; animation-name: slideInUp;">
                                <div class="bs-example bs-example-tabs" role="tabpanel" data-example-id="togglable-tabs">
                                    
                                    <div id="myTabContent" class="tab-content">
                                        <ul id="myTab" class="nav nav-tabs" role="tablist">
                                            <i class="fal fa-check-square fa-2x"></i>
                                            <li role="presentation" class="active ml-1 mt-1">
                                                <a href="#home" id="home-tab" role="tab" data-toggle="tab" aria-controls="home" aria-expanded="true">
                                                    <h5 class="text-primary">Overview</h5>
                                                </a>
                                            </li>
                                           
                                            <i class="fa-solid fa-hotel fa-2x ml-4"></i>
                                            <li class="ml-1 mt-1" role="presentation">
                                                <a href="#profile" role="tab" id="profile-tab" data-toggle="tab" aria-controls="profile">
                                                    <h5 class="text-primary">Rooms</h5>
                                                </a>
                                            </li>
                                        </ul>
                                        
                                        <div role="tabpanel" class="tab-pane fade in active bootstrap-tab-text" id="home" aria-labelledby="home">
                                            <div class="mt-3">
                                                <p class="mt-2"><strong>{{ $single_hotel->short_description }}</strong></p>
                                                <h4>Most popular facilities</h4>
                                                <hr>
                                                <table class="tours-tabs_table">
                                                    <tbody>
                                                        <tr>
                                                            <div class="mr-2">
                                                                <th class="text-black">WIFI</th>
                                                                <td><b class="text-black">{{ $single_hotel->wifi }}</b> </td>
                                                            </div>
                                                            <th></th>
                                                            <th></th>
                                                            <th></th>
                                                            <th></th>
                                                            <th></th>
                                                            <div class="ml-2">
                                                                <th class="text-black ml-3">Laundry</th>
                                                                <td><b class="text-black ml-3">{{ $single_hotel->laundry }}</b> </td>
                                                            </div>
                                                        </tr>
                                                        <tr></tr>
                                                        <tr>
                                                            <div class="mr-2">
                                                                <th class="text-black">Air Condition</th>
                                                                <td><b class="text-black">{{ $single_hotel->air_condition }}</b> </td>
                                                            </div>
                                                            <th></th>
                                                            <th></th>
                                                            <th></th>
                                                            <th></th>
                                                            <th></th>
                                                            <div class="ml-2">
                                                                <th class="text-black ml-3">Pool</th>
                                                                <td><b class="text-black ml-3">{{ $single_hotel->pool }}</b> </td>
                                                            </div>
                                                        </tr>
                                                         <tr></tr>
                                                        <tr>
                                                            <div class="mr-2">
                                                                <th class="text-black">Gym</th>
                                                                <td><b class="text-black">{{ $single_hotel->gym }}</b> </td>
                                                            </div>
                                                            <th></th>
                                                            <th></th>
                                                            <th></th>
                                                            <th></th>
                                                            <th></th>
                                                            <div class="ml-2">
                                                                <th class="text-black ml-3">Room Service</th>
                                                                <td><b class="text-black ml-3">{{ $single_hotel->room_service }}</b> </td>
                                                            </div>
                                                        </tr>
                                                        <tr></tr>
                                                        <tr>
                                                            <div class="mr-2">
                                                                <th class="text-black">Parking</th>
                                                                <td><b class="text-black">{{ $single_hotel->parking }}</b> </td>
                                                            </div>
                                                            <th></th>
                                                            <th></th>
                                                            <th></th>
                                                            <th></th>
                                                            <th></th>
                                                            <div class="ml-2">
                                                                <th class="text-black ml-3">Restaurant</th>
                                                                <td><b class="text-black ml-3">{{ $single_hotel->restaurant }}</b> </td>
                                                            </div>
                                                        </tr>
                                                        <tr></tr>
                                                        <tr>
                                                            <th class="text-black">Breakfast</th>
                                                            <td><b class="text-black">{{ $single_hotel->breakfast }}</b></td>
                                                        </tr>
                                                    </tbody>
                                                    </table>
                                                    <hr>
                                                        
                                            </div>
                                            <h4>Reviews</h4>
                                        </div>
            
                                        
                                        <div role="tabpanel" class="tab-pane fade bootstrap-tab-text" id="profile" aria-labelledby="profile-tab">
                                            <div mt-3>
                                                <h4 class="mt-4 text-center text-black"><u>Rooms</u></h4>
                                                <br>
                                                <br>
                                            </div>
                                            <table class="table table-white">
                                                <thead>
                                                  <tr>
                                                    <th scope="col">Room Type</th>
                                                    <th scope="col">Bed</th>
                                                    <th scope="col">Bed Type</th>
                                                    <th scope="col">Sleeps</th>
                                                    <th scope="col">Image</th>
                                                    <th scope="col">Price Per night</th>
                                                  </tr>
                                                </thead>
                                                <tbody>
                                                    @foreach ($rooms as $room)
                                                    <tr>
                                                        <td>{{ $room->title }}</td>
                                                        <td>{{ $room->bed }}</td>
                                                        <td>{{ $room->bed_type }}</td>
                                                        <td>
                                                                @for ($quantity = 1; $quantity <= $room->sleep; $quantity++)
                                                                <i class="fa fa-user" aria-hidden="true"></i>
                                                                @endfor
                                                        </td>
                                                        <td><img src="{{ asset("storage/$room->room_image") }}" width="200px"></td>
                                                        <td>{{ $room->price_per_night }}</td>
                                                        
                                                        <td class="col-3">
                                                          <div class="btn-group" role="group">
                                                            <a href="#" class="btn btn-primary btn-sm">Reserve</a>
                                                          </div>
                                                        </td>
                                                      </tr>
                                                    @endforeach  
                                                </tbody>
                                              </table>          
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                </div>
                <div class="inline-block col-lg-3 ">
                        <div class="textwidget"><h4>Need Discover Holidays Help?</h4>
                            <p>We would be more than happy to help you. Our team remains stand by for 24/7 at your service.</p>
                            <h3 style="color: #931e1e;">For Custom Package or Group Tour Contact us:</h3>
                            <p>                                <span></span></p>
                            <h3><i class="fa fa-phone"></i> +88 01883050240</h3>
                            <p></p>
                            <p><span><i class="fa fa-envelope"></i> info@travelistabd.com</span></p>
                        </div>
                        <div>
                            @foreach ($random_hotel as $hotel)
                            <div class="single-destination relative mt-2">
                                <div class="thumb relative">
                                    <div class="overlay overlay-bg"></div>
                                    <img class="img-fluid" style="height:230px;" src="{{ asset("storage/$hotel->featured_image") }}">
                                </div>
                                <div class="desc">	
                                    <a href="{{ url("single-hotel/$hotel->id") }}" class="price-btn">view</a>			
                                    <p><h6 class="text-warning">{{ $hotel->hotel_district }}</h6></p>			
                                </div>	
                            </div>
                    @endforeach	
                
                        </div>
                </div>
                
            </div>
            </div>	
            </section>
@endsection