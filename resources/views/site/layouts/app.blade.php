	<!DOCTYPE html>
	<html lang="zxx" class="no-js">
	<head>
		<!-- Mobile Specific Meta -->
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		{{-- jquery ajax --}}
		<meta name="csrf-token" content="{{ csrf_token() }}">
		<!-- Favicon-->
		<link rel="shortcut icon" href="{{ asset('site/img/fav.png') }}">
		<!-- Author Meta -->
		<meta name="author" content="colorlib">
		<!-- Meta Description -->
		<meta name="description" content="">
		<!-- Meta Keyword -->
		<meta name="keywords" content="">
		<!-- meta character set -->
		<meta charset="UTF-8">
		<!-- Site Title -->
		<title>Travel</title>
		{{-- <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous"> --}}

		<link href="{{ asset('/site/css/bootstrap.css') }}" rel="stylesheet" type="text/css" media="all" />
		<link href="{{ asset('/site/css/pignose.layerslider.css') }}" rel="stylesheet" type="text/css" media="all" />
		<link href="{{ asset('/site/css/style.css') }}" rel="stylesheet" type="text/css" media="all" />
		

		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0/css/all.min.css" integrity="sha512-9usAa10IRO0HhonpyAIVpjrylPvoDwiPUiKdWk5t3PyolY1cOd4DSE0Ga+ri4AuTroPR5aQvXU9xC6qOPnzFeg==" crossorigin="anonymous" referrerpolicy="no-referrer" />
		<link href="https://fonts.googleapis.com/css?family=Poppins:100,200,400,300,500,600,700" rel="stylesheet"> 
        <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous"/>
			<!--
			CSS
			============================================= -->
			<link rel="stylesheet" href="{{ asset('site/css/linearicons.css') }}">
			<link rel="stylesheet" href="{{ asset('site/css/font-awesome.min.css') }}">
			<link rel="stylesheet" href="{{ asset('site/css/bootstrap.css') }}">
			<link rel="stylesheet" href="{{ asset('site/css/magnific-popup.css') }}">
			<link rel="stylesheet" href="{{ asset('site/css/jquery-ui.css') }}">				
			<link rel="stylesheet" href="{{ asset('site/css/nice-select.css') }}">							
			<link rel="stylesheet" href="{{ asset('site/css/animate.min.css') }}">
			<link rel="stylesheet" href="{{ asset('site/css/owl.carousel.css') }}">				
			<link rel="stylesheet" href="{{ asset('site/css/main.css') }}">
		</head>
		<body>	
			<!-- #header -->
				<header id="header">
				  <div class="header-top">
					  <div class="container">
						<div class="row align-items-center">
							<div class="col-lg-6 col-sm-6 col-6 header-top-left">
								<ul>
									<li><a href="#">Visit Us</a></li>
									<li><a href="#">Buy Tickets</a></li>
								</ul>
							</div>
							<div class="col-lg-6 col-sm-6 col-6 header-top-right">
							  <div class="header-social">
								  <a href="#"><i class="fa fa-facebook"></i></a>
								  <a href="#"><i class="fa fa-twitter"></i></a>
							  </div>
							</div>
						</div>
					  </div>
				  </div>
				  <div class="container main-menu">
					  <div class="row align-items-center justify-content-between d-flex">
						<div id="logo">
						  <a href="index.html"><img src="{{ asset('site/img/logo.png') }}" alt="" title="" /></a>
						</div>
						<nav id="nav-menu-container" class="navbar-expand-lg">
						  <ul class="nav-menu">
							<li class="nav-item"><a href="index.html">Home</a></li>
							<li><a href="contact.html">Flights</a></li>
							<li><a href="{{ url('/all-package') }}">Packages</a></li>
							<li><a href="{{ url('/all-hotel') }}">Hotels</a></li>
							<li class="menu-has-children"><a href="">Blog</a>
							  <ul>
								<li><a href="{{ url('/all-blog') }}">Blog Home</a></li>
								<li><a href="blog-single.html">Blog Single</a></li>
							  </ul>
							</li>
							<li><a href="about.html">About Us</a></li>
							@if(Auth::check())
								<li class="menu-has-children"><a class="text-success">{{ Auth::user()->name }}</a>
								  <ul>
									<li>
									  <form action="{{ url('/logout') }}" method="POST">
										@csrf
										  <button type="submit" class="nav-link btn btn-outline-success">
											<i class="fas fa-sign-out-alt">Log-Out</i>
											</button>
									  </form>
									</li>
								  </ul>
								</li>
							@else 
							<li><a class="text-warning" 
								style="cursor: pointer"
								data-toggle="modal" 
								data-target="#registerModal"><i class="fas fa-user-plus mr-1"></i>Register</a>
							</li>
							<li>
								<a class="text-warning" 
								style="cursor: pointer" 
								data-toggle="modal" 
								data-target="#loginModal"><i class="fa fa-sign-in mr-1" aria-hidden="true"></i>Log-In</a>
							</li>
							@endif
						  </ul>
						</nav>
					  </div>
				  </div>
			  </header>

			  <!--login modal --> <!--end login -->
			
			{{-- register modal --}}{{-- end register --}}
			  
            @yield('content')
			<!-- start footer Area -->	
			@include('auth.login')
			@include('auth.register')	
			@include('site.layouts.footer')
			<!-- End footer Area -->	
			<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
			<script src="{{ asset('site/js/vendor/jquery-2.2.4.min.js') }}"></script>
			<script src="{{ asset('site/js/popper.min.js') }}"></script>
			<script src="{{ asset('site/js/vendor/bootstrap.min.js') }}"></script>			
			<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBhOdIF3Y9382fqJYt5I_sswSrEw5eihAA"></script>		
 			<script src="{{ asset('site/js/jquery-ui.js') }}"></script>					
  			<script src="{{ asset('site/js/easing.min.js') }}"></script>			
			<script src="{{ asset('site/js/hoverIntent.js') }}"></script>
			<script src="{{ asset('site/js/superfish.min.js') }}"></script>	
			<script src="{{ asset('site/js/jquery.ajaxchimp.min.js') }}"></script>
			<script src="{{ asset('site/js/jquery.magnific-popup.min.js') }}"></script>						
			<script src="{{ asset('site/js/jquery.nice-select.min.js') }}"></script>					
			<script src="{{ asset('site/js/owl.carousel.min.js') }}"></script>							
			<script src="{{ asset('site/js/mail-script.js') }}"></script>	
			<script src="{{ asset('site/js/main.js') }}"></script>
			<script src="{{ asset('/site/js/imagezoom.js') }}"></script>
			<script src="{{ asset('/site/js/jquery.flexslider.js') }}"></script>
			<script src="{{ asset('/site/js/simpleCart.min.js') }}"></script>
			<script type="text/javascript" src="{{ asset('/site/js/bootstrap-3.1.1.min.js') }}"></script>
			
			@yield('scripts')
			@yield('scripts1')
		</body>
	</html>