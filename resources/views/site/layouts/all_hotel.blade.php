@extends('site.layouts.app')

@section('content')
<section class="about-banner relative">
    <div class="overlay overlay-bg"></div>
    <div class="container">				
        <div class="row d-flex align-items-center justify-content-center">
            <div class="about-content col-lg-12">
                <h1 class="text-white">
                    Hotels				
                </h1>	
                <p class="text-white link-nav"><a href="index.html">Home </a>  <span class="lnr lnr-arrow-right"></span>  <a href="hotels.html"> Hotels</a></p>
            </div>	
        </div>
    </div>
</section>
<!-- End banner Area -->	

<!-- Start destinations Area -->
<section class="destinations-area section-gap">
    <div class="container">
        <div class="row d-flex justify-content-center">
            <div class="menu-content pb-40 col-lg-8">
                <div class="title text-center">
                    <h1 class="mb-10">Popular Hotels</h1>
                    <p>We all live in an age that belongs to the young at heart. Life that is becoming extremely fast, day to.</p>
                </div>
            </div>
        </div>						
        <div class="row">
           @foreach ($hotels as $hotel)
           <div class="col-lg-4">
            <div class="single-destinations">
                <div class="thumb">
                    <img style="height:220px;" src="{{ asset("storage/$hotel->featured_image") }}" alt="">
                </div>
                <div class="details">
                    <h4 class="d-flex justify-content-between">
                        <span>{{ $hotel->title }}</span>                              	
                        <div class="star">
                          {{ $hotel->star }}
                          <span>Star</span>
                            {{-- <span class="fa fa-star checked"></span> --}}
                        </div>	
                    </h4>
                    <p>
                        <b class="text-warning">{{ $hotel->hotel_district }}</b>   |   49 Reviews
                    </p>
                    <ul class="package-list">
                        <li class="d-flex justify-content-between align-items-center">
                            <span>Swimming pool</span>
                            <span>{{ $hotel->pool }}</span>
                        </li>
                        <li class="d-flex justify-content-between align-items-center">
                            <span>Gymnesium</span>
                            <span>{{ $hotel->gym }}</span>
                        </li>
                        <li class="d-flex justify-content-between align-items-center">
                            <span>Wi-fi</span>
                            <span>{{ $hotel->wifi }}</span>
                        </li>
                        <li class="d-flex justify-content-between align-items-center">
                            <span>Room Service</span>
                            <span>{{ $hotel->room_service }}</span>
                        </li>
                        <li class="d-flex justify-content-between align-items-center">
                            <span>Air Condition</span>
                            <span>{{ $hotel->air_condition }}</span>
                        </li>
                        <li class="d-flex justify-content-between align-items-center">
                            <span>Restaurant</span>
                            <span>{{ $hotel->restaurant }}</span>
                        </li>												
                        <li class="d-flex justify-content-between align-items-center">
                            <span></span>
                            <a href="{{ url("single-hotel/$hotel->id") }}" class="price-btn">View Details</a>
                        </li>													
                    </ul>
                </div>
            </div>
        </div>
           @endforeach
        </div>
    </div>	
</section>
<!-- End destinations Area -->


<!-- Start home-about Area -->
<section class="home-about-area">
    <div class="container-fluid">
        <div class="row align-items-center justify-content-end">
            <div class="col-lg-6 col-md-12 home-about-left">
                <h1>
                    Did not find your Package? <br>
                    Feel free to ask us. <br>
                    We‘ll make it for you
                </h1>
                <p>
                    inappropriate behavior is often laughed off as “boys will be boys,” women face higher conduct standards especially in the workplace. That’s why it’s crucial that, as women, our behavior on the job is beyond reproach. inappropriate behavior is often laughed.
                </p>
                <a href="#" class="primary-btn text-uppercase">request custom price</a>
            </div>
            <div class="col-lg-6 col-md-12 home-about-right no-padding">
                <img class="img-fluid" src="img/hotels/about-img.jpg" alt="">
            </div>
        </div>
    </div>	
</section>
<!-- End home-about Area -->

@endsection