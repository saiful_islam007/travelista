<div class="modal fade" id="registerModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Sign Up</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <form method="POST" id="registerForm">
            @csrf
            <div class="form-group">
              <label for="name">Name</label>
              <input  id="nameInput" type="text" class="form-control name" name="name" value="{{ old('name') }}"  aria-describedby="emailHelp" placeholder="Enter Name">

              <span class="invalid-feedback" role="alert" id="nameError">
                <strong></strong>
              </span>
            </div>
            
            {{-- @error('name')
            <p class="text-danger">{{ $message }}</p>
             @enderror --}}

            <div class="form-group">
              <label for="exampleInputEmail1">Email address</label>
              <input id="emailInput" type="email" class="form-control email" name="email" value="{{ old('email') }}" aria-describedby="emailHelp" placeholder="Enter email">
              
              <span class="invalid-feedback" role="alert" id="emailError">
                <strong></strong>
              </span>
            </div>
           

            <div class="form-group">
              <label for="phone">Mobile Number</label>
              <input id="mobileInput" type="tel" class="form-control mobile" name="mobile" value="{{ old('mobile') }}" aria-describedby="emailHelp" placeholder="Enter mobile number">

              <span class="invalid-feedback" role="alert" id="mobileError">
                <strong></strong>
              </span>
            </div>

            

            <div class="form-group">
              <label for="exampleInputPassword1">Password</label>
              <input type="password" id="passwordInput" name="password" class="form-control password"  placeholder="Password">
              <span class="invalid-feedback" role="alert" id="passwordError">
                <strong></strong>
              </span>
            </div>
           
            <div class="form-group">
              <label for="exampleInputPassword1">Confirm Password</label>
              <input type="password" id="password_confirmationInput" name="password_confirmation" class="form-control"  placeholder="Confirm Password">
              <span class="invalid-feedback" role="alert" id="password_confirmationError">
                <strong></strong>
              </span>
            </div>
            
            <button type="submit" class="btn btn-primary add_user">Register</button>
          </form>
        </div>
        <div class="modal-footer">
          <a href="{{ route('login') }}">Already register?Log-in</a>
        </div>
      </div>
    </div>
  </div>


@section('scripts')
@parent

<script>
$(function () {
    $('#registerForm').submit(function (e) {
        e.preventDefault();
        let formData = $(this).serializeArray();
        $(".invalid-feedback").children("strong").text("");
        $("#registerForm input").removeClass("is-invalid");
        $.ajax({
            method: "POST",
            headers: {
                Accept: "application/json"
            },
            url: "{{ route('register') }}",
            data: formData,
            success: () => window.location.assign("{{ route('site.home') }}"),
            error: (response) => {
                if(response.status === 422) {
                    let errors = response.responseJSON.errors;
                    Object.keys(errors).forEach(function (key) {
                        $("#" + key + "Input").addClass("is-invalid");
                        $("#" + key + "Error").children("strong").text(errors[key][0]);
                    });
                } else {
                    window.location.reload();
                }
            }
        })
    });
}) 
</script>
@endsection 
{{-- action="{{ route('register') }}" --}}


{{-- my jaqury --}}
{{-- @section('scripts1')
  <script>
    $(document).ready(function () {
      $(document).on('click', '.add_user', function (e) {
        e.preventDefault();

        var data={
          'name':$('.name').val(),
          'email':$('.email').val(),
          'mobile':$('.mobile').val(),
          'password':$('.password').val(),
        }

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax({
          type: "POST",
          url: "{{ route('register') }}",
          data: data,
          dataType: "json",
          success: function (response) {
            
          }
        });

       
      });
    });
  </script>
@endsection --}}