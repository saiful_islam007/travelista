<div class="modal fade" id="loginModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Log-In</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <form method="POST" action="{{ route('login') }}">
            @csrf
            <div class="form-group">
              <label for="exampleInputEmail1">Email address</label>
              <input type="email" class="form-control" name="email" value="{{ old('email') }}" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email">
            </div>
            @error('email')
            <p class="text-danger">{{ $message }}</p>
             @enderror
            <div class="form-group">
              <label for="exampleInputPassword1">Password</label>
              <input type="password" class="form-control" name="password" id="exampleInputPassword1" placeholder="Password">
            </div>
            @error('password')
            <p class="text-danger">{{ $message }}</p>
             @enderror
            <div class="block mt-4">
              <label for="remember_me" class="inline-flex items-center">
                  <input id="remember_me" type="checkbox" class="rounded border-gray-300 text-indigo-600 shadow-sm focus:border-indigo-300 focus:ring focus:ring-indigo-200 focus:ring-opacity-50" name="remember">
                  <span class="ml-2 text-sm text-gray-600">{{ __('Remember me') }}</span>
              </label>
          </div>

            <button type="submit" class="btn btn-primary">Log-In</button>
          </form>
        </div>
        <div class="modal-footer">
          <a href="#">Register a new membership</a>
        </div>
      </div>
    </div>
  </div>

  @section('scripts')
  @parent
  
  @if($errors->has('email') || $errors->has('password'))
      <script>
      $(function() {
          $('#loginModal').modal({
              show: true
          });
      });
      </script>
  @endif
  @endsection