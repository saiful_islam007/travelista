<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHotelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hotels', function (Blueprint $table) {
            $table->id();
            $table->string('title');
            $table->text('location');
            $table->double('star');
            $table->text('short_description')->nullable();
            $table->string('wifi');
            $table->string('air_condition');
            $table->string('pool');
            $table->string('gym');
            $table->string('room_service');
            $table->string('parking');
            $table->string('laundry');
            $table->string('restaurant');
            $table->string('breakfast');
            $table->string('featured_image')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('hotels');
    }
}
